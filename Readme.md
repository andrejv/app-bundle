APP-BUNDLE
===========

`app-bundle` is a common lisp package for creating application bundles for OSX. It creates a minimal application buddle required for creating GUI applications in common lisp. Currently supports `sbcl`, `ccl` and `cmucl`.

##### Example

```
CL-USER> (asdf:load-system :your-package)
CL-USER> (asdf:load-system :app-bundle)
CL-USER> (app-bundle:make "Application.app" :toplevel #'your-package:main)
```
