;;;
;;; Copyright (c) 2015 Andrej Vodopivec <andrej.vodopivec@gmail.com>
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy
;;; of this software and associated documentation files (the "Software"), to deal
;;; in the Software without restriction, including without limitation the rights
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;;; copies of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in
;;; all copies or substantial portions of the Software.
;;; 
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;;; THE SOFTWARE.
;;;

(in-package :app-bundle)

(defun write-entry (s key str)
  (format s "  <key>~a</key>~%" key)
  (format s "  <string>~a</string>~%" str))

(defun write-header (s)
  (format s "<?xml version=\"1.0\" encoding=\"UTF-8\"?>~%")
  (format s "<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">~%")
  (format s "<plist version=\"1.0\">~%")
  (format s " <dict>~%"))

(defun write-footer (s)
  (format s " </dict>~%")
  (format s "</plist>~%"))

(defun get-user ()
  #+ccl (ccl:getenv "USER")
  #+cmu (cdr (assoc :USER ext:*environment-list*))
  #+sbcl (sb-posix:getenv "USER")
  #-(or ccl sbcl cmu) "")

(defun save-app (path toplevel compress)
  (declare (ignorable compress))
  #+ccl (ccl:save-application path :prepend-kernel t :toplevel-function toplevel)
  #+cmu (ext:save-lisp path :executable t
                       :init-function (lambda () (funcall toplevel) (ext:quit))
                       :print-herald nil)
  #+sbcl (sb-ext:save-lisp-and-die path :executable t :toplevel toplevel :compression compress)
  #-(or ccl cmu sbcl) (error "Not supported"))

(defun make (bundlepath &key toplevel
                          (version "0.1") (short-version "Version 0.1")
                          copyright author icns-file
                          (compress t)
                          (lisp-info t))
  (let* ((bundle-name  (pathname-name bundlepath))
         (bundle-dir   (pathname-directory (pathname-as-directory bundlepath)))
         (bundle-cont  (append bundle-dir (list "Contents")))
         (bundle-macos (append bundle-cont (list "MacOS")))
         (bundle-res   (append bundle-cont (list "Resources"))))

    (unless icns-file
      (setf icns-file (merge-pathnames "Lisp.icns" (or #.*compile-file-pathname* #.*load-pathname*))))
    (unless copyright
      (setf copyright (format nil "Copyright (C) ~a" (or author (get-user)))))

    (when lisp-info
      (setf version (format nil "~a-~a-~a" version (lisp-implementation-type) (lisp-implementation-version))))
    
    (when (probe-file bundlepath)
      (delete-directory-and-files bundlepath))
    
    (ensure-directories-exist (make-pathname :directory bundle-res))
    (ensure-directories-exist (make-pathname :directory bundle-macos))

    (with-open-file (s (make-pathname :directory bundle-cont :name "Pkginfo")
                       :direction :output)
      (format s "APPL????"))


    (with-open-file (s (make-pathname :directory bundle-cont :name "Info" :type "plist")
                       :direction :output)
      (write-header s)
      (write-entry s "CFBundleExecutable" bundle-name)
      (write-entry s "CFBundlePackageType" "APPL")
      (when version
        (write-entry s "CFBundleVersion" version))
      (when short-version
        (write-entry s "CFBundleShortVersionString" short-version))
      (when copyright
        (write-entry s "NSHumanReadableCopyright" copyright))
      (when (and icns-file (probe-file icns-file))
        (let ((icns-file-name (make-pathname :name (pathname-name icns-file)
                                             :type (pathname-type icns-file))))
          (write-entry s "CFBundleIconFile" icns-file-name)
          (copy-file icns-file
                     (make-pathname :directory bundle-res
                                    :name (pathname-name icns-file)
                                    :type (pathname-type icns-file)))))
      (write-footer s))

    (save-app (make-pathname :directory bundle-macos :name bundle-name)
              toplevel compress)))

